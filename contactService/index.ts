// Start your code here!
// You should not need to edit any other existing files (other than if you would like to add tests)
// You do not need to import anything as all the necessary data and events will be delivered through
// updates and service, the 2 arguments to the constructor
// Feel free to add files as necessary

interface Contact {
    firstName: string;
    lastName: string;
    nickName: string;
    primaryPhoneNumber: string;
    secondaryPhoneNumber: string;
    primaryEmail: string;
    secondaryEmail: string;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    city: string;
    state: string;
    zipCode: string;
    id: string;
}

interface FormattedContact {
    name: string;
    phones: string[];
    email: string;
    address: string;
    id: string;
}

export default class {

    contacts: Contact[]

    constructor(updates: { emit: Function, on: Function }, service: { getById: Function; }) {
        this.contacts = [];

        // cache contact on 'add'
        updates.on('add', async (id: string) => this.contacts.push(await service.getById(id)))
        // update cached contact on 'change'
        updates.on('change',
            async (id: string, field: string, value: string) => this.changeContact(id, field, value)
        
        )
        // remove contact from cache on 'remove'
        updates.on('remove', async (id: string) => this.removeContact(id))
    }

    search(query: string): FormattedContact[] {

        const matches: FormattedContact[] = [];

        for (const contact of this.contacts) {

            let pushed = false;

            const values = Object.values(contact)

            // loop through contact values
            for (const value of values) {
                // normalize value and query then attempt to match
                if (value.replace(/[^a-z0-9]/gi,'').includes(query.replace(/[^a-z0-9]/gi,''))) {
                    matches.push(this.formatContact(contact));
                    pushed = true;
                    break;
                }
            }

            // if matched, continue to next iteration
            if (pushed) {
                continue;
            }

            // if a match hasn't been found, search by name variation
            if (this.searchByNameVariation(query, `${contact.firstName} ${contact.nickName} ${contact.lastName}`)) {
                matches.push(this.formatContact(contact))
            }
        }
        
        return matches;
    }
    
    formatContact(contact: Contact): FormattedContact {

        const formattedContact = {
            // if nickname exists, return instead of first name
            name: contact.nickName !== '' ? `${contact.nickName} ${contact.lastName}` : `${contact.firstName} ${contact.lastName}`,
            // generate array of uniform phone numbers
            phones: this.formatPhoneNumbers([contact.primaryPhoneNumber, contact.secondaryPhoneNumber]),
            email: contact.primaryEmail,
            // consolidate address into single string
            address: this.formatAddress([
                contact.addressLine1,
                contact.addressLine2,
                contact.addressLine3,
                contact.city,
                contact.state,
                contact.zipCode
            ]),
            id: contact.id
        }

        return formattedContact;
    }

    formatAddress(addressDataArray: string[]): string {
        
        let address: string = '';
        
        // build address string
        for (const data of addressDataArray) {
            // if value exits, concat
            if (data !== '') {
                address += `${data}, `
            }
        }

        // remove scruff from end of string
        if (address[address.length - 1] === ',') {
           address = address.slice(0, address.length - 2)
        }

        return address;
    }

    formatPhoneNumbers(unformattedPhoneNumberArray: string[]): string[] {

        // phone numbers are expected to be returned in an array
        const formattedPhoneNumberArray: string[] = [];

        // but they must be formatted correctlt first
        for (let phoneNumber of unformattedPhoneNumberArray) {
            if (phoneNumber !== '') {
                formattedPhoneNumberArray.push(this.formatPhoneNumber(phoneNumber))
            }
        }

        return formattedPhoneNumberArray;
    }

    formatPhoneNumber(phoneNumber: string): string {

        // remove country code and all special characters
        let pn = phoneNumber.replace('+1', '').replace(/\D/g, '');

        // return properly formatted phone number
        return `(${pn[0]}${pn[1]}${pn[2]}) ${pn[3]}${pn[4]}${pn[5]}-${pn[6]}${pn[7]}${pn[8]}${pn[9]}`
    }

    changeContact(id: string, field: string, value: string): void {

        // find contact by id
        for (let i = 0; i < this.contacts.length; i++) {
            if (id === this.contacts[i].id) {
                // update value
                this.contacts[i][field] = value;
            }
        }
    }

    removeContact(id: string): void {

        // find contact by id
        for (let i = 0; i < this.contacts.length; i++) {
            if (id === this.contacts[i].id) {
                // remove from cache
                this.contacts.splice(i, 1)
            }
        }
    }

    searchByNameVariation(query: string, contactFullName: string): boolean {

        // break query string into specific words
        const queryStringArray: string[] = query.split(' ');

        // return false if any query string words don't exist in contact full name
        for (const word of queryStringArray) {
            if (!contactFullName.includes(word)) {
                return false;
            }
        }

        return true;
    }
}
